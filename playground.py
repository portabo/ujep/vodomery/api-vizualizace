def get_binary(hex: str) -> str:
    return "{0:08b}".format(int(hex, 16))


errors = "00000200"
bin = ""

n = 2
hexes = [(errors[i : i + n]) for i in range(0, len(errors), n)]

for hex in hexes:
    bin += get_binary(hex)

for idx, b in enumerate(bin):
    if int(b):
        print(2**idx)
