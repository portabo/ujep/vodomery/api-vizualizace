from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .routers import notifications, records, watermeters

app = FastAPI(
    title="UJEP Voda – API",
    description="API pro projekt ujep-voda.",
    version="1.0.1",
    contact={
        "name": "UJEP",
        "url": "https://ujep.cz/",
        "email": "heidlerjosef@gmail.com",
    },
    license_info={"name": "MIT", "url": "https://choosealicense.com/licenses/mit/"},
    redoc_url=None,
    docs_url="/",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET"],
    allow_headers=["*"],
)

app.include_router(records.router)
app.include_router(watermeters.router)
app.include_router(notifications.router)
