from typing import Any

import pytz
from pydantic import BaseSettings, Field, SecretStr, validator  # type: ignore
from tzlocal import get_localzone


def normalize_none(env_value: str) -> Any | None:
    if env_value == "":
        return None
    return env_value


class InfluxSettings(BaseSettings):
    org: str
    bucket: str
    token: SecretStr
    host: str
    port: int

    def get_url(self):
        return f"http://{self.host}:{self.port}"


class AppSettings(BaseSettings, extra="ignore"):
    timezone: str | None  # Nikde není využito.
    debug: bool = True
    password: SecretStr

    _normalize_none = validator("*", pre=True, allow_reuse=True)(normalize_none)  # type: ignore

    @validator("timezone", pre=True)
    def validate_timezone(cls, v: str | None) -> str:
        if v is None or v == "":
            v = str(get_localzone())

        try:
            pytz.timezone(v)
        except Exception as e:
            raise ValueError(f"Invalid timezone: {v}") from e

        return v


class EmailSettings(BaseSettings):
    address: str = Field(alias="name")
    password: SecretStr
    deadman_address: str = Field(alias="deadman")
    threshold_address: str = Field(alias="threshold")


class Settings(BaseSettings):
    influx: InfluxSettings
    app: AppSettings
    email: EmailSettings

    class Config:  # type: ignore
        env_file = ".env"
        case_sensitive = False
        env_file_encoding = "utf-8"
        env_nested_delimiter = "__"


settings = Settings()  # type: ignore
