from datetime import datetime

import pytz
from pydantic import BaseModel, Field, validator

from ..settings import settings


class Deadman(BaseModel):
    dt_notification: datetime = Field(alias="_status_timestamp")
    name: str = Field(alias="_check_name")
    level: str = Field(alias="_level")
    sensor: str = Field(alias="id")

    @validator("dt_notification")
    def convert_to_timezone(cls, v):
        if settings.app.timezone is not None:
            return v.astimezone(pytz.timezone(settings.app.timezone))


class Threshold(BaseModel):
    dt_notification: datetime = Field(alias="_status_timestamp")
    name: str = Field(alias="_check_name")
    level: str = Field(alias="_level")
    sensor: str = Field(alias="id")

    @validator("dt_notification")
    def convert_to_timezone(cls, v):
        if settings.app.timezone is not None:
            return v.astimezone(pytz.timezone(settings.app.timezone))
