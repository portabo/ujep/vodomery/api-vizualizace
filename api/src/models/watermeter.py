from enum import Enum

from pydantic import BaseModel, parse_obj_as


class WatermeterManufacturer(str, Enum):
    KAMSTRUP = "kamstrup"
    MADDALENA = "maddalena"


class WatermeterConnection(str, Enum):
    ETHERNET = "ethernet"
    LORA = "lora"


class Watermeter(BaseModel):
    id: str
    name: str
    manufacturer: WatermeterManufacturer
    connection: WatermeterConnection


watermeters = [
    {
        "id": "pata",
        "name": "Patní vodoměr",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.ETHERNET,
    },
    {
        "id": "cpto",
        "name": "CPTO",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.ETHERNET,
    },
    {
        "id": "kampus_h",
        "name": "FSI",
        "manufacturer": WatermeterManufacturer.MADDALENA,
        "connection": WatermeterConnection.ETHERNET,
    },
    {
        "id": "vk",
        "name": "Vědecká knihovna",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.ETHERNET,
    },
    {
        "id": "menza",
        "name": "Menza",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.ETHERNET,
    },
    {
        "id": "klisska_28",
        "name": "Klíšská 28",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.LORA,
    },
    {
        "id": "klisska_30",
        "name": "Klíšská 30",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.LORA,
    },
    {
        "id": "kampus_j",
        "name": "Vila J",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.ETHERNET,
    },
    {
        "id": "kampus_t",
        "name": "Kampus T",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.LORA,
    },
    {
        "id": "ff_b",
        "name": "Filozofická fakulta (A)",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.ETHERNET,
    },
    {
        "id": "ff_a",
        "name": "Filozofická fakulta (B)",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.LORA,
    },
    {
        "id": "mfc",
        "name": "MFC",
        "manufacturer": WatermeterManufacturer.KAMSTRUP,
        "connection": WatermeterConnection.ETHERNET,
    },
]

watermeters = parse_obj_as(list[Watermeter], watermeters)
