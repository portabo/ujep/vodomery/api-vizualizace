import math
from datetime import datetime
from decimal import Decimal
from enum import Enum
from typing import Any

from pydantic import BaseModel, Field, validator  # type: ignore


def value_validator(value: Decimal) -> Decimal | None:
    if value or value == 0:
        return round(Decimal(value), 2)
    return None


class Aggregator(str, Enum):  # TODO: Přidat pouze funkce, které dávají smysl.
    COUNT = "count"
    MEAN = "mean"
    MEDIAN = "median"
    LAST = "last"
    FIRST = "first"
    MAX = "max"
    MIN = "min"


class StatusError(str, Enum):
    NO_ERROR = "00"
    PERMANENT_ERROR = "08"
    TEMPORARY_ERROR = "10"
    PERMANENT_AND_TEMPORARY_ERROR = "18"


class Record(BaseModel):
    id: str
    manufacturer: str = Field(alias="_measurement", exclude=True)
    status: StatusError | None
    dt: datetime = Field(alias="_time")
    actual_flow: Decimal | None = Field(decimal_places=2, exclude=False)
    mean_flow: Decimal | None = Field(decimal_places=2)
    max_flow: Decimal | None = Field(decimal_places=2)
    min_flow: Decimal | None = Field(decimal_places=2)
    errors: list[Any] | None
    volume: Decimal | None = Field(decimal_places=2)
    max_total_flow: Decimal | None = Field(decimal_places=2, exclude=True)
    max_monthly_flow: Decimal | None = Field(decimal_places=2, exclude=True)
    timestamp: datetime | None = Field(exclude=True)

    _normalize_values = validator("actual_flow", "mean_flow", "max_flow", "min_flow", "volume", "max_total_flow", "max_monthly_flow", allow_reuse=True, pre=True)(value_validator)  # type: ignore

    @validator("status", pre=True)
    def status_validator(cls, value: str, values: dict[str, Any]) -> str | None:
        if value:
            if values["manufacturer"] == "kamstrup":
                if value:
                    return StatusError(value)
            elif values["manufacturer"] == "maddalena":
                if not value == "00":
                    return "UNSPECIFIED_ERROR"
        return value

    @classmethod
    def _get_errors(cls, hex_string: str, n_chars: int = 2) -> list[int]:
        hexes = [
            (hex_string[i : i + n_chars]) for i in range(0, len(hex_string), n_chars)
        ]

        binaries = ""

        for hex in hexes:
            binaries += cls._get_binary(hex)

        errors = []
        for idx, b in enumerate(binaries):
            if int(b):
                error = 2**idx
                errors.append(error)

        return errors

    @classmethod
    def _get_binary(cls, hex: str) -> str:
        return "{0:08b}".format(int(hex, 16))

    @validator("errors", pre=True)
    def error_validator(cls, value: str, values: dict[str, Any]) -> list[Any] | None:
        if values["manufacturer"] == "kamstrup" and value:
            errors = cls._get_errors(value, 2)

        elif values["manufacturer"] == "maddalena" and value:
            errors = [value]  # type: ignore
        else:
            errors = None
        return errors

    class Config:
        json_encoders = {  # type: ignore
            StatusError: lambda v: v.name,  # type: ignore
        }
        schema_extra = {
            "example": {
                "id": "cpto",
                "manufacturer": "kamstrup",
                "status": "TEMPORARY_ERROR",
                "dt": "2023-07-29T09:52:11.176244+00:00",
                "actual_flow": 60,
                "errors": [64, 512, 1024],
                "volume": 686.71,
                "max_total_flow": 1205,
                "max_monthly_flow": 1004,
                "timestamp": "2023-07-29T08:51:00+00:00",
            }
        }
