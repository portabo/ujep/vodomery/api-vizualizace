from influxdb_client import InfluxDBClient

from .settings import settings

INFLUX_TIMEOUT = 60000


def get_influx_client():
    client = InfluxDBClient(
        url=settings.influx.get_url(),
        token=settings.influx.token.get_secret_value(),
        org=settings.influx.org,
        timeout=INFLUX_TIMEOUT,
        debug=settings.app.debug,
    )
    try:
        yield client
    finally:
        client.close()
