from fastapi import HTTPException, Security, status
from fastapi.security.api_key import APIKeyHeader

from .settings import settings

api_key_header = APIKeyHeader(name="secret_key", auto_error=False)


def check_secret_key(api_key_header: str = Security(api_key_header)):
    if api_key_header and api_key_header in settings.app.password.get_secret_value():
        return api_key_header
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="API klíč nebyl ověřen.",
        )
