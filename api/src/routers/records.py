import json
from datetime import datetime
from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, Path, Query, status
from fastapi.openapi.models import APIKey
from influxdb_client import InfluxDBClient
from pydantic import parse_obj_as

from ..auth import check_secret_key
from ..database import get_influx_client, settings
from ..models.record import Aggregator, Record

START_ANNOTATION = Annotated[
    datetime | None,
    Query(
        title="start",
        description="Datum a čas, od kdy mají být data načtena. Ve formátu ISO (2023-07-28T10:00:00Z).",
    ),
]
STOP_ANNOTATION = Annotated[
    datetime | None,
    Query(
        title="stop",
        description="Datum a čas, do kdy mají být data načtena. Ve formátu ISO (2023-07-28T18:00:00Z).",
    ),
]
AGG_ANNOTATION = Annotated[
    str | None,
    Query(
        title="agg",
        description="Agregování dat, kdy dojde k seskupení záznamů (např. 30s, 1h, 1d, 2w, 3m, 1y). V daném časovém intervalu se vezme poslední záznam.",
    ),
]

# AGG_FN_ANNOTATION = Annotated[
#     Aggregator,
#     Query(
#         title="agg_fn",
#         description="Specifikováno, jaká funkce se má použít pro agregaci dat. Funkční pouze v kombinaci s parametrem agg.",
#     ),
# ]

router = APIRouter(prefix="/records", tags=["Records"])


def get_query(
    id: str | None,
    start: datetime | None,
    stop: datetime | None,
    agg: str | None,
    agg_fn: Aggregator,
) -> str:
    query = f"""data = from(bucket: "{settings.influx.bucket}")"""

    if start:
        start_query = f"start: {start.isoformat()}"
    else:
        start_query = "start: 0"

    if stop:
        stop_query = f"stop: {stop.isoformat()}"
    else:
        stop_query = ""

    if not start and not stop:
        last = '|> last(column: "_time")'
    else:
        last = ""

    query = (
        query
        + f"""
        |> range({start_query}, {stop_query})
        {last}
        |> keep(columns: ["_field", "id", "status", "_time", "_value", "_measurement"])
        """
    )

    if id:
        query = query + f'|> filter(fn: (r) => r["id"] == "{id}")'

    if agg and (start or stop):
        flow = "mean_flow"
        query = (
            query
            + f"""
            actualFlowMean =
                data
                    |> filter(fn: (r) => r._field == "actual_flow")
                    |> aggregateWindow(every: {agg}, fn: mean, createEmpty: false)
                    |> set(key: "_field", value: "{flow}")

            actualFlowMax =
                data
                    |> filter(fn: (r) => r._field == "actual_flow")
                    |> aggregateWindow(every: {agg}, fn: max, createEmpty: false)
                    |> set(key: "_field", value: "max_flow")

            actualFlowMin =
                data
                    |> filter(fn: (r) => r._field == "actual_flow")
                    |> aggregateWindow(every: {agg}, fn: min, createEmpty: false)
                    |> set(key: "_field", value: "min_flow")

            dataAgg =
                data
                    |> aggregateWindow(every: {agg}, fn: last, createEmpty: false)

            union(tables: [actualFlowMean, actualFlowMax, actualFlowMin, dataAgg])
            """
        )
    else:
        flow = "actual_flow"

    query = (
        query
        + f"""
            |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
            |> group(columns: ["id"])
            |> sort(columns: ["_time"])
            |> keep(
                    columns: [
                        "id",
                        "status",
                        "_time",
                        "_measurement",
                        "{flow}",
                        "max_flow",
                        "min_flow",
                        "errors",
                        "volume",
                    ],
                )
            """
    )

    if not start and not stop:
        query = (
            query
            + f"""
            |> last(column: "_time")
            |> yield(name: "data")
            """
        )
    else:
        query = (
            query
            + f"""
            |> yield(name: "data")
            """
        )

    return query


@router.get(
    "/{id}",
    response_model=list[Record],
    response_model_by_alias=False,
    summary="Získej záznamy z vodoměru podle ID",
    description="Pokud není zadán *start* a *stop*: bude vzat poslední záznam (bez výpočtu *mean_flow*) a zároveň nefunguje agregace.",
)
def get_records_by_id(
    id: Annotated[str, Path(title="id", description="Jméno (ID) vodoměru.")],
    start: START_ANNOTATION = None,
    stop: STOP_ANNOTATION = None,
    agg: AGG_ANNOTATION = None,
    # agg_fn: AGG_FN_ANNOTATION = Aggregator.LAST,
    db: InfluxDBClient = Depends(get_influx_client),
    secret_key: APIKey = Depends(check_secret_key),
):
    query = get_query(id, start, stop, agg, Aggregator.MEAN)

    results = db.query_api().query(query=query).to_json()  # type: ignore
    results = json.loads(results)

    if not results:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Záznamy s parametry id: {id}, start: {start}, stop: {stop}, agg: {agg} nebyly nalezeny.",
        )
    return results


@router.get(
    "/",
    response_model=list[Record],
    response_model_by_alias=False,
    summary="Získej záznamy z vodoměrů",
    description="Pokud není zadán *start* a *stop*: bude vzat poslední záznam (bez výpočtu *consumption* a *mean_flow*) a zároveň nefunguje agregace.",
)
def get_records(
    start: START_ANNOTATION = None,
    stop: STOP_ANNOTATION = None,
    agg: AGG_ANNOTATION = None,
    # agg_fn: AGG_FN_ANNOTATION = Aggregator.LAST,
    db: InfluxDBClient = Depends(get_influx_client),
    secret_key: APIKey = Depends(check_secret_key),
):
    query = get_query(None, start, stop, agg, Aggregator.MEAN)

    results = db.query_api().query(query=query).to_json()  # type: ignore
    results = json.loads(results)

    if not results:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Záznamy s parametry start: {start}, stop: {stop}, agg: {agg} nebyly nalezeny.",
        )

    return results
