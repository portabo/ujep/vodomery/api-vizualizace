from fastapi import APIRouter, Depends, HTTPException, Path, Query, status
from fastapi.openapi.models import APIKey

from ..auth import check_secret_key
from ..models.watermeter import Watermeter, watermeters

router = APIRouter(prefix="/watermeters", tags=["Watermeters"])


@router.get(
    "/",
    response_model=list[Watermeter],
    response_model_by_alias=False,
    summary="Získej vodoměry",
)
def get_watermeters(
    secret_key: APIKey = Depends(check_secret_key),
):
    results = watermeters

    if not results:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Vodoměry neexistují.",
        )

    return results
