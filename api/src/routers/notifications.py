import notifiers
from fastapi import APIRouter, Depends, Request
from fastapi.openapi.models import APIKey

from ..auth import check_secret_key
from ..models.notification import Deadman, Threshold
from ..settings import settings

router = APIRouter(prefix="/notifications", tags=["Notifications"])

params = {
    "username": settings.email.address,
    "password": settings.email.password.get_secret_value(),
    "from": "vodomery@ujep.cz",
}

notifier = notifiers.get_notifier("gmail")


@router.post(
    "/deadman/",
    response_model=Deadman,
    response_model_by_alias=False,
)
def deadman(
    notification: Deadman,
    # secret_key: APIKey = Depends(check_secret_key),
):
    notifier.notify(
        message=f"{notification.name.upper()} ({notification.dt_notification})\n\nVodoměr: {notification.sensor.upper()}\nZpráva: Vodoměr přestal posílat záznamy.",
        subject=f"Vodoměry UJEP: Notifikace - Deadman",
        to=settings.email.deadman_address,
        **params,
    )

    return notification


@router.post(
    "/threshold/",
    response_model=Threshold,
    response_model_by_alias=False,
)
def threshold(
    notification: Threshold,
    # secret_key: APIKey = Depends(check_secret_key),
):
    notifier.notify(
        message=f"{notification.name.upper()} ({notification.dt_notification})\n\nVodoměr: {notification.sensor.upper()}\nZpráva: Vodoměr přesáhl zvolené hranice.",
        subject=f"Vodoměry UJEP: Notifikace - Threshold",
        to=settings.email.threshold_address,
        **params,
    )

    return notification
