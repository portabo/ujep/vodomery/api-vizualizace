from datetime import date, timedelta

import streamlit as st
from models.settings import settings
from models.watermeter import get_last_records, get_watermeters

MAIN_WATERMETER_ID = "pata"
API_URL = "http://api:8087"
# API_URL = "http://127.0.0.1:8000/"
# API_URL = "http://d4.ci.ujep.cz:8087/"


def setup_session(title: str):
    st.session_state.api_url = API_URL

    st.set_page_config(
        page_title=title,
        page_icon=":potable_water:",
        initial_sidebar_state="auto",
        layout="wide",
        menu_items={"About": f"API: {st.session_state.api_url}"},
    )

    if "settings" not in st.session_state:
        st.session_state.settings = settings

    if "agg" not in st.session_state:
        st.session_state.agg = "1h"
        st.session_state.agg_index = 1

    if ("to_date" not in st.session_state) and ("from_date" not in st.session_state):
        today = date.today()
        st.session_state.to_date = today + timedelta(days=1)
        st.session_state.from_date = today
        # Čas by šlo udělat lépe, aby tam byl i první interval, ale bylo by nutné brát vždy o ten jeden interval víc (tzn. dříve), a to v závislosti na agregaci.

    if "main_watermeter" not in st.session_state:
        st.session_state.watermeter_main_id = MAIN_WATERMETER_ID

    if "watermeter_detail_id" not in st.session_state:
        st.session_state.watermeter_detail_id = MAIN_WATERMETER_ID

    if "watermeters" not in st.session_state:
        st.session_state.last_records = get_last_records(st.session_state.settings)
        st.session_state.watermeters = get_watermeters()
