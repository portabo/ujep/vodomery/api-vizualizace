import plotly.express as px
import streamlit as st
from auth import check_password
from components.sidebar import sidebar
from models.settings import settings
from models.watermeter import update_detail
from setup import setup_session
from utils import format_selectbox_options

PAGE_TITLE = "Detailní přehled"
title = PAGE_TITLE

setup_session(title)

if check_password():
    sidebar()

    options = [watermeter.id for watermeter in st.session_state.watermeters]
    watermeter_detail_id = options.index(st.session_state.watermeter_detail_id)

    st.selectbox(
        ":mag: Zvolte vodoměr",
        options,
        index=watermeter_detail_id,
        format_func=format_selectbox_options,
        key="input_watermeter",
        help="Vyberte vodoměr pro který chcete zobrazit detailní přehled.",
        on_change=update_detail,
        args=None,
        kwargs=None,
        placeholder="Vyberte...",
        disabled=False,
        label_visibility="visible",
    )
    st.divider()

    if "input_watermeter":
        watermeter = next(
            (
                i
                for i in st.session_state.watermeters
                if i.id == st.session_state.input_watermeter
            ),
            None,
        )

        if watermeter:
            st.title(watermeter.name)
            st.caption(
                f"**ID:** {watermeter.id} | **Výrobce:** {watermeter.manufacturer.title()} | **Konektivita**: {watermeter.connection.title()}"
            )

            if watermeter.stats:
                st.write(
                    f"**Poslední záznam:** {watermeter.stats.dt.strftime('%d.%m.%Y %H:%M')} | **Stavové hlášení:** {watermeter.stats.errors.status_emoji} | **Nulový průtok:** {watermeter.stats.flow_stop}"
                )

                st.divider()

                with st.container():
                    col_1, col_2, col_3, col_4, col_5 = st.columns(5)

                    with col_1:
                        st.metric(
                            "Stav vodoměru [m3]",
                            value=watermeter.stats.volume.last,
                            help=":question: Aktuální stav vodoměru.",
                            label_visibility="visible",
                        )

                    with col_2:
                        st.metric(
                            "Aktuální průtok [m3/h]",
                            value=watermeter.stats.actual_flow,
                            help=":question: Aktuální průtok v posledním záznamu.",
                            label_visibility="visible",
                        )

                    with col_3:
                        min_flow_interval = round(
                            watermeter.stats.min_flow.data["min_flow"].max(), 2
                        )

                        st.metric(
                            "Min. průtok [m3]",
                            value=min_flow_interval,
                            delta_color="normal",
                            help=":question: Minimální průtok ve sledovaném intervalu.",
                            label_visibility="visible",
                        )

                    with col_4:
                        max_flow_interval = round(
                            watermeter.stats.max_flow.data["max_flow"].max(), 2
                        )

                        st.metric(
                            "Max. průtok [m3]",
                            value=max_flow_interval,
                            delta_color="normal",
                            help=":question: Maximální průtok ve sledovaném intervalu.",
                            label_visibility="visible",
                        )

                    with col_5:
                        volume_total_interval = round(
                            watermeter.stats.volume.data["volume"]
                            .iloc[[0, -1]]
                            .diff()
                            .values[1],
                            2,
                        )

                        st.metric(
                            "Spotřeba [m3]",
                            value=volume_total_interval,
                            delta_color="normal",
                            help=":question: Celková spotřeba ve sledovaném intervalu.",
                            label_visibility="visible",
                        )

                with st.container():
                    plotly_config = {"displaylogo": False}

                    # volume_interval = watermeter.stats.volume.data[
                    #     ["dt", "consumption"]
                    # ].copy()
                    # volume_interval.columns = ["Datum a čas", "Spotřeba [m3]"]

                    # period_flow_fig = px.bar(
                    #     volume_interval,
                    #     x="Datum a čas",
                    #     y="Spotřeba [m3]",
                    #     hover_data={"Datum a čas": "%d.%m.%Y %H:%M"},
                    # )
                    # period_flow_fig.update_xaxes(
                    #     tickformat="%d.%m.%Y<br>%H:%M",
                    #     title=None,
                    # )
                    # period_flow_fig.update_traces(
                    #     hovertemplate="Datum: %{x|%Y-%m-%d}<br>"
                    #     "Čas: %{x|%H:%M}<br>"
                    #     "Spotřeba [m3]: %{y}",
                    # )

                    # st.plotly_chart(
                    #     period_flow_fig,
                    #     config=plotly_config,
                    #     use_container_width=True,
                    #     sharing="streamlit",
                    #     theme="streamlit",
                    # )

                    flow_interval = watermeter.stats.flow.data.copy()
                    flow_interval.columns = ["Datum a čas", "Průtok [m3/h]"]

                    period_flow_fig = px.line(
                        flow_interval,
                        x="Datum a čas",
                        y="Průtok [m3/h]",
                        hover_data={"Datum a čas": "%d.%m.%Y %H:%M"},
                    )
                    period_flow_fig.update_xaxes(
                        tickformat="%d.%m.%Y<br>%H:%M",
                        title=None,
                    )
                    period_flow_fig.update_traces(
                        hovertemplate="Datum: %{x|%Y-%m-%d}<br>"
                        "Čas: %{x|%H:%M}<br>"
                        "Průtok [m3/h]: %{y}",
                    )

                    st.plotly_chart(
                        period_flow_fig,
                        config=plotly_config,
                        use_container_width=True,
                        sharing="streamlit",
                        theme="streamlit",
                    )

                st.divider()

                st.header(":loudspeaker: Přehled chybových hlášení")

                errors_table_data = watermeter.stats.errors.data.copy()
                errors_table_data = errors_table_data.loc[
                    errors_table_data["status"] != "NO_ERROR"
                ]

                errors_table_data["status"] = (
                    errors_table_data["status_emoji"]
                    + " "
                    + errors_table_data["status"]
                )
                errors_table_data = errors_table_data.loc[
                    errors_table_data["status"].notnull()
                ]
                errors_table_data.sort_index(ascending=False, inplace=True)

                if errors_table_data.empty:
                    st.write(
                        ":white_check_mark: Ve sledovaném intervalu nebyly zaznamenány žádné chyby."
                    )
                else:
                    config = {
                        "dt": st.column_config.DatetimeColumn(
                            label="Čas hlášení",
                            help=None,
                            disabled=True,
                            format="DD.MM.YYYY HH:mm",
                            step=60,
                            timezone=settings.timezone,
                        ),
                        "status": st.column_config.TextColumn(
                            label="Typ hlášení",
                            help=None,
                            disabled=True,
                        ),
                        "error_code": st.column_config.NumberColumn(
                            label="Kód chyby",
                            help=None,
                            format="%.0f",
                            disabled=True,
                        ),
                        "error_msg": st.column_config.TextColumn(
                            label="Text chyby",
                            help=None,
                            disabled=True,
                        ),
                    }

                    st.dataframe(
                        data=errors_table_data,
                        use_container_width=True,
                        hide_index=True,
                        column_config=config,  # type: ignore
                        column_order=[
                            "dt",
                            "status",
                            "error_code",
                            "error_msg",
                        ],
                    )
            else:
                st.subheader(
                    ":heavy_exclamation_mark: Data pro požadovaný interval nebyla nalezena."
                )
