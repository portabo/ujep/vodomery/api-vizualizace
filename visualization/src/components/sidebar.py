from datetime import timedelta

import pandas as pd
import streamlit as st
from models.settings import settings
from models.watermeter import refresh_data, update_agg, update_interval
from utils import data_to_excel, format_interval_options, get_download_filename


def sidebar():
    with st.sidebar:
        st.header("Nastavení")

        st.date_input(
            "Sledovaný interval",
            value=(
                st.session_state.from_date,
                st.session_state.to_date - timedelta(days=1),
            ),
            min_value=None,
            max_value=None,
            key="input_interval",
            help=":question: Zadejte interval, který chcete sledovat.",
            on_change=update_interval,
            args=None,
            kwargs=None,
            format="DD.MM.YYYY",
            disabled=False,
            label_visibility="visible",
        )

        st.divider()

        st.radio(
            "Agregace dat",
            options=("15m", "1h", "1d", "1w", "1mo"),
            index=st.session_state.agg_index,
            format_func=format_interval_options,
            key="input_aggregator",
            help=":question: Zvolte agregaci dat.",
            on_change=update_agg,
            args=None,
            kwargs=None,
            disabled=False,
            horizontal=False,
            label_visibility="visible",
        )

        st.divider()

        st.subheader("Aktualizace dat")
        st.write(
            "Data jsou automaticky aktualizována při změně intervalu / agregaci dat. Pokud chcete data aktualizovat ručně, klikněte na tlačítko níže."
        )
        st.button(
            "Aktualizovat",
            key="refresh",
            help=None,
            on_click=refresh_data,
            args=None,
            kwargs=None,
            type="primary",
            disabled=False,
            use_container_width=True,
        )

        st.divider()

        st.header("Stažení dat")
        st.write(
            f"Data budou stažena v excelovském formátu (XLSX) dle zvoleného intervalu a agregace pro všechny vodoměry. Časová zóna je **{settings.timezone}**."
        )

        data = [watermeter.data for watermeter in st.session_state.watermeters]
        data = pd.concat(data, axis=0)
        data["date"] = data["dt"].dt.strftime("%d.%m.%Y")
        data["time"] = data["dt"].dt.strftime("%H:%M:%S")
        data = data[
            [
                "id",
                "date",
                "time",
                "status",
                "volume",
                "mean_flow",
                "max_flow",
                "consumption",
            ]
        ]

        def create_last_rows(watermeter):
            df = pd.DataFrame.from_dict(
                dict(id=watermeter.id, **watermeter.stats.dict())
            )
            df = df.stack().to_frame().T
            df = df.reset_index(drop=True)
            df.columns = [f"{i}_{j}" for i, j in df.columns]
            return df

        stats = [
            create_last_rows(watermeter)
            for watermeter in st.session_state.watermeters
            if watermeter.stats is not None
        ]

        stats = pd.concat(stats, axis=0)
        stats["last_dt"] = pd.to_datetime(stats["last_dt"])
        stats["last_date"] = stats["last_dt"].dt.strftime("%d.%m.%Y")
        stats["last_time"] = stats["last_dt"].dt.strftime("%H:%M:%S")
        stats = stats[
            [
                "last_id",
                "last_date",
                "last_time",
                "last_volume",
                "penultimate_volume",
                "last_flow",
                "penultimate_flow",
                "last_max_flow",
                "penultimate_max_flow",
                "last_actual_flow",
                # "penultimate_actual_flow",
                "status_msg_errors",
            ]
        ]
        stats.sort_values(by="last_volume", inplace=True, ascending=False)
        stats = stats.transpose()

        errors = [
            watermeter.stats.errors.data if watermeter.stats else None
            for watermeter in st.session_state.watermeters
        ]
        errors = pd.concat(errors, axis=0)
        errors["date"] = errors["dt"].dt.strftime("%d.%m.%Y")
        errors["time"] = errors["dt"].dt.strftime("%H:%M:%S")
        errors = errors[["id", "date", "time", "status", "error_code"]]

        file = data_to_excel(data, errors, stats)

        st.download_button(
            "Stáhnout",
            file,
            file_name=get_download_filename(
                st.session_state.from_date, st.session_state.to_date
            ),
            mime="text/plain",
            key="download",
            help=None,
            on_click=None,
            args=None,
            kwargs=None,
            disabled=False,
            use_container_width=True,
        )
