import pytz
from pydantic import BaseSettings, Field, SecretStr, validator
from tzlocal import get_localzone


class Settings(BaseSettings):
    timezone: str = Field(env="APP__TIMEZONE")
    password: SecretStr = Field(env="APP__PASSWORD")

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = False

    @validator("timezone", pre=True)
    def validate_timezone(cls, v: str | None) -> str:
        if v is None or v == "":
            v = str(get_localzone())

        try:
            pytz.timezone(v)
        except Exception as e:
            raise ValueError(f"Invalid timezone: {v}") from e

        return v


settings = Settings()
