import urllib.parse
from datetime import date, datetime, timedelta
from typing import Any

import emoji
import pandas as pd
import requests
import streamlit as st
from pydantic import BaseModel, ConfigDict, Field
from utils import format_date, format_interval_options_index

from .settings import Settings

# KAMSTRUP_DETAILED_ERRORS = {
#     0: "No error",
#     8: "Temperature sensor T1 above range or disconnected",
#     16: "Temperature sensor T2 above range or disconnected",
#     32: "Temperature sensor T1 below range or disconnected",
#     64: "Temperature sensor T2 below range or disconnected",
#     1048576: "Temperature sensor T3 above range or disconnected",
#     2097152: "Temperature sensor T3 below range or disconnected",
#     4: "External alarm",
#     128: "Wrong differential temperature T1-T2",
#     256: "Air detected in flow sensor V1",
#     512: "Reverse flow in flow sensor V1",
#     1024: "Signal too low in flow sensor V1",
#     2048: "Very high flow in flow sensor V1 for more than 1 hour",
#     4096: "Water leakage detected on Pulse input A (A1)",
#     8192: "Water leakage detected on Pulse input B (B1)",
#     16384: "External alarm on Pulse input A (A1)",
#     32768: "External alarm on Pulse input B (B1)",
#     65536: "Flow sensor V1 communication error",
#     131072: "Flow sensor V1 pulse error",
#     262144: "Water leakage detected on Pulse input A2",
#     524288: "Water leakage detected on Pulse input B2",
#     4194304: "Flow sensor V2 communication error",
#     8388608: "Flow sensor V2 pulse error",
#     16777216: "Air detected in flow sensor V2",
#     33554432: "Reverse flow in flow sensor V2",
#     67108864: "Signal too low in flow sensor V2",
#     134217728: "Very high flow in flow sensor V2 for more than 1 hour",
#     268435456: "V1V2 burst out of the system",
#     536870912: "V1V2 burst into the system",
#     1073741824: "V1V2 leakage out of the system",
#     2147483648: "V1V2 leakage into the system",
# }

KAMSTRUP_DETAILED_ERRORS = {
    0: "Žádná chyba",
    8: "Snímač teploty T1 nad rozsahem nebo odpojený",
    16: "Snímač teploty T2 nad rozsahem nebo odpojený",
    32: "Snímač teploty T1 pod rozsahem nebo odpojený",
    64: "Snímač teploty T2 pod rozsahem nebo odpojený",
    1048576: "Snímač teploty T3 nad rozsahem nebo odpojený",
    2097152: "Snímač teploty T3 pod rozsahem nebo odpojený",
    4: "Externí alarm",
    128: "Chybná diferenční teplota T1-T2",
    256: "Vzduch detekovaný ve snímači průtoku V1",
    512: "Zpětný tok v průtokovém snímači V1",
    1024: "Signál je příliš nízký ve snímači průtoku V1",
    2048: "Velmi vysoký průtok v průtokovém čidle V1 po dobu delší než 1 hodinu",
    4096: "Na pulzním vstupu A (A1) byl zjištěn únik vody",
    8192: "Na pulzním vstupu B (B1) byl zjištěn únik vody",
    16384: "Externí alarm na pulzním vstupu A (A1)",
    32768: "Externí alarm na pulzním vstupu B (B1)",
    65536: "Chyba komunikace snímače průtoku V1",
    131072: "Chyba pulzu snímače průtoku V1",
    262144: "Na pulzním vstupu A2 zjištěn únik vody",
    524288: "Na pulzním vstupu B2 zjištěn únik vody",
    4194304: "Chyba komunikace snímače průtoku V2",
    8388608: "Chyba pulzu snímače průtoku V2",
    16777216: "Vzduch detekován ve snímači průtoku V2",
    33554432: "Zpětný tok v průtokovém snímači V2",
    67108864: "Příliš nízký signál v průtokovém snímači V2",
    134217728: "Velmi vysoký průtok v průtokovém čidle V2 po dobu delší než 1 hodinu",
    268435456: "V1V2 vypadl ze systému",
    536870912: "V1V2 vtrhla do systému",
    1073741824: "Únik V1V2 ze systému",
    2147483648: "Únik V1V2 do systému",
}

STATUS = {
    "NO_ERROR": emoji.emojize(":ok:", language="alias"),
    "PERMANENT_ERROR": emoji.emojize(":warning:", language="alias"),
    "TEMPORARY_ERROR": emoji.emojize(":loudspeaker:", language="alias"),
    "PERMANENT_AND_TEMPORARY_ERROR": f"{emoji.emojize(':warning:', language='alias')} {emoji.emojize(':loudspeaker:', language='alias')}",
    "UNSPECIFIED_ERROR": emoji.emojize(":x:", language="alias"),
    None: emoji.emojize(":grey_question:", language="alias"),
}

ERROR_SKIP = ["cpto", "menza", "klisska_28", "klisska_30"]
ERRORS_TO_SKIP = [4194304]


class WaterValue(BaseModel):
    last: float | None
    penultimate: float | None
    data: Any | None = Field(repr=False, exclude=True)


class WaterErrors(BaseModel):
    status_msg: str | None
    status_emoji: str | None = Field(repr=False)
    data: Any | None = Field(repr=False, exclude=True)


class Stats(BaseModel):
    flow: WaterValue | None
    volume: WaterValue | None
    max_flow: WaterValue | None
    min_flow: WaterValue | None
    dt: datetime | None
    actual_flow: float | None
    errors: WaterErrors | None
    flow_stop: str | None


class Watermeter(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)

    id: str
    name: str
    manufacturer: str
    connection: str
    data: Any = Field(repr=False)
    stats: Stats | None

    def get_data(
        self, from_date: date, to_date: date, agg: str, settings: Settings
    ) -> Any:
        from_date = format_date(from_date)  # type: ignore
        to_date = format_date(to_date)  # type: ignore

        headers = {
            "secret_key": settings.password.get_secret_value(),
        }

        url = urllib.parse.urljoin(
            st.session_state.api_url,
            f"/records/{self.id}?start={from_date}&stop={to_date}&agg={agg}",
        )
        response = requests.get(url=url, headers=headers)

        try:
            data = pd.DataFrame.from_records(response.json())
            data.sort_values(by="dt", inplace=True)
            data = data.round(2)
            data["dt"] = pd.to_datetime(data["dt"])
            data["dt"] = data["dt"].dt.tz_convert(settings.timezone)
            data["consumption"] = data["volume"].diff()

        except Exception as e:
            data = None

        self.data = data

        return data

    def get_stats(self, settings: Settings, last_record: pd.DataFrame | None):
        if isinstance(self.data, pd.DataFrame) and not self.data.empty:
            flow = self.data[["dt", "mean_flow"]]
            max_flow = self.data[["dt", "max_flow"]]
            min_flow = self.data[["dt", "min_flow"]]
            volume = self.data[["dt", "volume", "consumption"]]

            try:
                flow_penultimate = flow.iloc[-2]["mean_flow"]
                max_flow_penultimate = max_flow.iloc[-2]["max_flow"]
                min_flow_penultimate = min_flow.iloc[-2]["min_flow"]
                volume_penultimate = volume.iloc[-2]["volume"]
                flow_stop = min_flow[["min_flow"]].min().values[0]

                if flow_stop == 0:
                    flow_stop = "Ano"
                elif flow_stop > 0:
                    flow_stop = "Ne"
                else:
                    flow_stop = None
            except:
                flow_penultimate = None
                max_flow_penultimate = None
                min_flow_penultimate = None
                volume_penultimate = None
                flow_stop = None

            flow = WaterValue(
                last=flow.iloc[-1]["mean_flow"],
                penultimate=flow_penultimate,
                data=flow,
            )

            max_flow = WaterValue(
                last=max_flow.iloc[-1]["max_flow"],
                penultimate=max_flow_penultimate,
                data=max_flow,
            )

            min_flow = WaterValue(
                last=min_flow.iloc[-1]["min_flow"],
                penultimate=min_flow_penultimate,
                data=min_flow,
            )

            volume = WaterValue(
                last=volume.iloc[-1]["volume"],
                penultimate=volume_penultimate,
                data=volume,
            )

            errors = self.data[["id", "dt", "status", "errors"]].copy()

            # if self.id in ["klisska_30", "klisska_28", "kampus_t", "ff_a"]:
            #     print(errors)

            errors = errors.explode("errors")
            errors["status_emoji"] = errors["status"].map(STATUS)
            errors.rename(columns={"errors": "error_code"}, inplace=True)
            errors["error_msg"] = errors["error_code"].map(KAMSTRUP_DETAILED_ERRORS)

            if self.id in ERROR_SKIP:
                errors = errors[~errors["error_code"].isin(ERRORS_TO_SKIP)]

            if not errors.empty:
                last_status = errors.iloc[-1]
            else:
                last_status = {
                    "status": "NO_ERROR",
                    "status_emoji": STATUS.get("NO_ERROR"),
                }

            errors = WaterErrors(
                status_msg=last_status["status"],
                status_emoji=last_status["status_emoji"],
                data=errors,
            )

            if isinstance(last_record, pd.DataFrame) and not last_record.empty:
                dt = last_record[["dt"]].iloc[-1].values[0]
                dt = pd.to_datetime(dt).tz_localize("UTC").tz_convert(settings.timezone)

                actual_flow = last_record[["actual_flow"]].iloc[-1].values[0]
            else:
                dt = None
                errors = None
                actual_flow = None

            stats = Stats(
                flow=flow,
                max_flow=max_flow,
                min_flow=min_flow,
                volume=volume,
                dt=dt,
                actual_flow=actual_flow,
                errors=errors,
                flow_stop=flow_stop,
            )

            self.stats = stats

            return stats

    def get_simplified_data(self):
        if isinstance(self.stats, Stats):
            return {
                "id": self.id,
                "dt": self.stats.dt,
                "name": self.name,
                "volume": self.stats.volume.last,
                "flow": self.stats.flow.last,
                "max_flow": self.stats.max_flow.last,
                "min_flow": self.stats.max_flow.last,
                "actual_flow": self.stats.actual_flow,
                "data_flow": self.stats.flow.data[
                    self.stats.flow.data["mean_flow"].notnull()
                ]["mean_flow"].to_list(),
                "flow_stop": self.stats.flow_stop,
                "volume_total_interval": round(
                    self.stats.volume.data["volume"].iloc[[0, -1]].diff().values[1],
                    2,
                ),
                "data_volume": self.stats.volume.data[
                    self.stats.volume.data["volume"].notnull()
                ]["volume"].to_list(),
                "status": self.stats.errors.status_emoji,
            }
        else:
            return {
                "id": self.id,
                "dt": None,
                "name": self.name,
                "volume": None,
                "flow": None,
                "max_flow": None,
                "min_flow": None,
                "actual_flow": None,
                "data_flow": None,
                "volume_total_interval": None,
                "data_volume": None,
                "status": STATUS[None],
            }


def get_watermeters() -> list[Watermeter]:
    last_records = st.session_state.last_records
    URL = urllib.parse.urljoin(st.session_state.api_url, "/watermeters/")

    headers = {
        "secret_key": st.session_state.settings.password.get_secret_value(),
    }

    response = requests.get(url=URL, headers=headers)
    watermeters = response.json()

    watermeters_objs = []
    for watermeter in watermeters:
        watermeter_obj = Watermeter(**watermeter)
        watermeter_obj.get_data(
            st.session_state.from_date,
            st.session_state.to_date,
            st.session_state.agg,
            st.session_state.settings,
        )

        last_record = last_records.loc[last_records["id"] == watermeter_obj.id]

        watermeter_obj.get_stats(st.session_state.settings, last_record)

        if watermeter_obj.id == st.session_state.watermeter_main_id:
            st.session_state.watermeter_main = watermeter_obj

        watermeters_objs.append(watermeter_obj)

    return watermeters_objs


def update_interval():
    if len(st.session_state.input_interval) == 2:
        st.session_state.from_date = st.session_state.input_interval[0]
        st.session_state.to_date = st.session_state.input_interval[1] + timedelta(
            days=1
        )

        st.session_state.watermeters = get_watermeters()


def update_agg():
    st.session_state.agg = st.session_state.input_aggregator
    st.session_state.agg_index = format_interval_options_index(st.session_state.agg)
    st.session_state.watermeters = get_watermeters()


def update_detail():
    st.session_state.watermeter_detail_id = st.session_state.input_watermeter


def refresh_data():
    st.session_state.last_records = get_last_records(st.session_state.settings)
    st.session_state.watermeters = get_watermeters()


def get_last_records(settings: Settings):
    headers = {
        "secret_key": settings.password.get_secret_value(),
    }

    url = urllib.parse.urljoin(
        st.session_state.api_url,
        f"/records/",
    )
    response = requests.get(url=url, headers=headers)

    try:
        data = pd.DataFrame.from_records(response.json())
        data.sort_values(by="dt", inplace=True)
        data = data.round(2)
        data["dt"] = pd.to_datetime(data["dt"])
        data["dt"] = data["dt"].dt.tz_convert(settings.timezone)

    except Exception as e:
        data = None

    return data
