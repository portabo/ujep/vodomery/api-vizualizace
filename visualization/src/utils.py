from datetime import date, datetime
from io import BytesIO

import pandas as pd
import pytz
import streamlit as st
from models.settings import settings


def format_selectbox_options(option: str) -> str | None:
    return next((i.name for i in st.session_state.watermeters if i.id == option), None)


def format_interval_options(option: str) -> str:
    match option:
        case "15m":
            value = "15 min"
        case "1h":
            value = "1 hod"
        case "1d":
            value = "1 den"
        case "1w":
            value = "1 týden"
        case "1mo":
            value = "1 měsíc"

    return value


def format_interval_options_index(option: str) -> int:
    match option:
        case "15m":
            value = 0
        case "1h":
            value = 1
        case "1d":
            value = 2
        case "1w":
            value = 3
        case "1mo":
            value = 4

    return value


def format_date(date: date) -> str:
    new_date = (
        datetime(date.year, date.month, date.day)
        .astimezone(tz=pytz.timezone(settings.timezone))
        .astimezone(tz=pytz.timezone("UTC"))
        .replace(tzinfo=None)
        .isoformat(timespec="seconds")
    )

    return new_date + "Z"


def get_download_filename(from_date: date, to_date: date) -> str:
    from_date_str = from_date.strftime("%d%m%Y")
    to_date_str = to_date.strftime("%d%m%Y")

    return f"voda_ujep_{from_date_str}-{to_date_str}.xlsx"


def data_to_excel(data, errors, stats):
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine="xlsxwriter")
    data.to_excel(writer, sheet_name="data", index=False, na_rep="")
    errors.to_excel(writer, sheet_name="errors", index=False, na_rep="")
    stats.to_excel(writer, sheet_name="stats", index=True, na_rep="", header=False)
    writer.close()
    processed_data = output.getvalue()

    return processed_data
