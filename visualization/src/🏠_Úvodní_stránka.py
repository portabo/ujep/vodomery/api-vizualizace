from datetime import date, datetime, timedelta

import pandas as pd
import plotly.express as px
import streamlit as st
from auth import check_password
from components.sidebar import sidebar
from models.settings import settings
from setup import setup_session

PAGE_TITLE = "Správa vodoměrů"
PAGE_SUBTITLE = "Univerzita J. E. Purkyně v Ústí nad Labem"
title = f"{PAGE_TITLE}: {PAGE_SUBTITLE}"

setup_session(title)


if check_password():
    sidebar()

    with st.container():
        # st.title(PAGE_TITLE)
        # st.subheader(PAGE_SUBTITLE)
        # st.divider()

        st.header(":potable_water: Přehled vodoměrů")
        st.write(
            ":question: Uvedené hodnoty se vztahují k intervalu zvoleném v postranním panelu."
        )

        table_data = []
        plot_flow_data = []
        for watermeter in st.session_state.watermeters:
            table_data.append(watermeter.get_simplified_data())

            if watermeter.stats is not None:
                plot_data = watermeter.stats.flow.data.copy()
                plot_data["name"] = watermeter.name
                plot_flow_data.append(plot_data)

        plot_flow_data = pd.concat(plot_flow_data)

        table_data = pd.DataFrame(table_data)
        table_data.set_index("id", inplace=True, drop=True)  # type: ignore
        table_data.sort_values(by="volume", inplace=True, ascending=False)  # type: ignore
        table_data["dt_ago"] = pd.Timestamp.now(tz="UTC").round("1min") - table_data["dt"].dt.round("1min")  # type: ignore
        table_data["dt_ago"] = (
            table_data["dt_ago"][table_data["dt_ago"].notna()]
            .dt.total_seconds()
            .apply(lambda x: int(x // 60))
        )

        config = {
            # "id": st.column_config.TextColumn(
            #     label="ID",
            #     help=None,
            #     disabled=True,
            # ),
            "name": st.column_config.TextColumn(
                label="Místo",
                help=None,
                disabled=True,
            ),
            "status": st.column_config.TextColumn(
                label="Stavové hlášení",
                help=None,
                disabled=True,
            ),
            "actual_flow": st.column_config.NumberColumn(
                label="Aktuální průtok [m3/h]",
                help=None,
                disabled=True,
                format="%.2f",
            ),
            "flow_stop": st.column_config.TextColumn(
                label="Nulový průtok",
                help=None,
                disabled=True,
            ),
            "volume": st.column_config.NumberColumn(
                label="Stav vodoměru [m3]",
                help=None,
                disabled=True,
                format="%.2f",
            ),
            "volume_total_interval": st.column_config.NumberColumn(
                label="Spotřeba [m3]",
                help=None,
                disabled=True,
                format="%.2f",
            ),
            "data_flow": st.column_config.LineChartColumn(
                label="Trend průtoku [m3/h]",
                width="medium",
                help=None,
            ),
            # "data_volume": st.column_config.LineChartColumn(
            #     label="Trend spotřeby [m3]",
            #     width="medium",
            #     help=None,
            # ),
            "dt": st.column_config.DatetimeColumn(
                label="Poslední záznam",
                help=None,
                disabled=True,
                format="DD.MM.YYYY HH:mm",
                step=60,
                timezone=settings.timezone,
            ),
            "dt_ago": st.column_config.NumberColumn(
                label="Před [min]",
                help=None,
                disabled=True,
            ),
        }

        st.dataframe(
            data=table_data,
            use_container_width=True,
            hide_index=True,
            column_config=config,  # type: ignore
            column_order=[
                # "id",
                "name",
                "volume",
                "actual_flow",
                "flow_stop",
                "data_flow",
                "volume_total_interval",
                # "data_volume",
                "status",
                "dt",
                "dt_ago",
            ],
        )

        st.caption(
            ":ok: Bez chyby | :grey_question: Není známo | :loudspeaker: Dočasná chyba | :warning: Permanentní chyba | :x: Nespecifikovaná chyba"
        )

        st.divider()

        st.header(
            "Celková spotřeba",
        )

        if st.session_state.watermeter_main and st.session_state.watermeter_main.stats:
            st.caption(
                f"**ID:** {st.session_state.watermeter_main.id} | **Výrobce:** {st.session_state.watermeter_main.manufacturer.title()} | **Konektivita**: {st.session_state.watermeter_main.connection.title()}"
            )
            st.write(
                f"**Poslední záznam:** {st.session_state.watermeter_main.stats.dt.strftime('%d.%m.%Y %H:%M')} | **Stavové hlášení:** {st.session_state.watermeter_main.stats.errors.status_emoji}"
            )

            st.divider()

            with st.container():
                col_1, col_2, col_3, col_4, col_5 = st.columns(5)

                with col_1:
                    st.metric(
                        "Stav vodoměru [m3]",
                        value=st.session_state.watermeter_main.stats.volume.last,
                        help=":question: Aktuální stav vodoměru.",
                        label_visibility="visible",
                    )

                with col_2:
                    st.metric(
                        "Aktuální průtok [m3/h]",
                        value=st.session_state.watermeter_main.stats.actual_flow,
                        help=":question: Aktuální průtok v posledním záznamu.",
                        label_visibility="visible",
                    )

                with col_3:
                    min_flow_interval = round(
                        st.session_state.watermeter_main.stats.min_flow.data[
                            "min_flow"
                        ].max(),
                        2,
                    )

                    st.metric(
                        "Min. průtok [m3]",
                        value=min_flow_interval,
                        delta_color="normal",
                        help=":question: Minimální průtok ve sledovaném intervalu.",
                        label_visibility="visible",
                    )

                with col_4:
                    max_flow_interval = round(
                        st.session_state.watermeter_main.stats.max_flow.data[
                            "max_flow"
                        ].max(),
                        2,
                    )

                    st.metric(
                        "Max. průtok [m3]",
                        value=max_flow_interval,
                        delta_color="normal",
                        help=":question: Maximální průtok ve sledovaném intervalu.",
                        label_visibility="visible",
                    )

                with col_5:
                    volume_total_interval = round(
                        st.session_state.watermeter_main.stats.volume.data["volume"]
                        .iloc[[0, -1]]
                        .diff()
                        .values[1],
                        2,
                    )

                    st.metric(
                        "Spotřeba [m3]",
                        value=volume_total_interval,
                        delta_color="normal",
                        help=":question: Celková spotřeba ve sledovaném intervalu.",
                        label_visibility="visible",
                    )

                # st.write(
                #     f":sparkles: **Interval:** {st.session_state.from_date.strftime('%d.%m.%Y')}–⁠{(st.session_state.to_date - timedelta(days=1)).strftime('%d.%m.%Y')}"
                # )

            with st.container():
                plotly_config = {"displaylogo": False}

                # volume_interval = st.session_state.watermeter_main.stats.volume.data[
                #     ["dt", "consumption"]
                # ].copy()
                # volume_interval.columns = ["Datum a čas", "Spotřeba [m3]"]

                # period_flow_fig = px.bar(
                #     volume_interval,
                #     x="Datum a čas",
                #     y="Spotřeba [m3]",
                #     hover_data={"Datum a čas": "%d.%m.%Y %H:%M"},
                # )
                # period_flow_fig.update_xaxes(
                #     tickformat="%d.%m.%Y<br>%H:%M",
                #     title=None,
                # )
                # period_flow_fig.update_traces(
                #     hovertemplate="Datum: %{x|%Y-%m-%d}<br>"
                #     "Čas: %{x|%H:%M}<br>"
                #     "Spotřeba [m3]: %{y}",
                # )

                # st.plotly_chart(
                #     period_flow_fig,
                #     config=plotly_config,
                #     use_container_width=True,
                #     sharing="streamlit",
                #     theme="streamlit",
                # )

                plot_flow_data.columns = ["Datum a čas", "Průtok [m3/h]", "Místo"]

                period_flow_fig = px.line(
                    plot_flow_data,
                    x="Datum a čas",
                    y="Průtok [m3/h]",
                    hover_data={"Datum a čas": "%d.%m.%Y %H:%M"},
                    color="Místo",
                )
                period_flow_fig.update_xaxes(
                    tickformat="%d.%m.%Y<br>%H:%M",
                    title=None,
                )
                period_flow_fig.update_traces(
                    hovertemplate="Datum: %{x|%Y-%m-%d}<br>"
                    "Čas: %{x|%H:%M}<br>"
                    "Průtok [m3/h]: %{y}",
                )

                st.plotly_chart(
                    period_flow_fig,
                    config=plotly_config,
                    use_container_width=True,
                    sharing="streamlit",
                    theme="streamlit",
                )

        else:
            st.write("Problém s načtením dat patního vodoměru.")

        st.divider()
